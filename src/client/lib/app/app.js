// Generated by CoffeeScript 1.7.1
require(["moo_router", "navbar", "moo", "text_handler"], function(moo_router, navbar, moo, text_handler) {
  window.App = {};
  App.Views = {};
  App.moo_router = new moo_router;
  App.Views.navbar = new navbar;
  App.Views.text_handler = new text_handler;
  return Backbone.history.start();
});
